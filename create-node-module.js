#!/usr/bin/env node

/**
 * @file create-node-module.js
 * @author L0th1
 */

import {readdir, stat, copyFile, readFile, writeFile, mkdir} from 'fs/promises';
import {execFile} from 'child_process';
import {promisify} from 'util';
import {fileURLToPath} from 'url';
import {dirname, join} from 'path';
const __dirname = dirname(fileURLToPath(import.meta.url));

/**
 *
 */
async function main() {
  await promisify(execFile)('npm', ['init', '-y']);
  const packageJson = JSON.parse((await readFile('./package.json')).toString());
  packageJson.type = 'module';
  packageJson.scripts = {
    verify: 'npx tsc -p verify.jsconfig.json',
    buildDeclaration: [
      'rm -f index.d.ts',
      "find lib -name '*.d.ts' -delete",
      'npx tsc -p declaration.jsconfig.json',
    ].join(' && '),
    ...packageJson.scripts,
  };
  await writeFile('./package.json', JSON.stringify(packageJson));
  for (const dir of ['lib', 'tests', 'typings']) {
    try {
      await mkdir(dir);
    } catch (err) {
      if (err.code !== 'EEXIST') {
        throw err;
      }
      if (!(await stat(dir)).isDirectory()) {
        throw new Error(`${dir} exists and is not a directory`);
      }
    }
  }

  await writeFile(
    './jsconfig.json',
    JSON.stringify({
      compilerOptions: {
        module: 'ESNext',
        target: 'ESNext',
        checkJs: true,
        moduleResolution: 'node',
        resolveJsonModule: true,
      },
      include: ['index.js', 'lib', 'tests', 'typings'],
    })
  );
  await writeFile(
    './verify.jsconfig.json',
    JSON.stringify({
      extends: './jsconfig.json',
      compilerOptions: {
        declaration: true,
        noEmit: true,
      },
    })
  );
  await writeFile(
    './declaration.jsconfig.json',
    JSON.stringify({
      extends: './jsconfig.json',
      include: ['index.js', 'lib'],
      exclude: ['node_modules', 'tests'],
      compilerOptions: {
        declaration: true,
        noEmit: false,
        emitDeclarationOnly: true,
      },
    })
  );
  const basePath = join(__dirname, './files');
  for (const file of await readdir(basePath)) {
    await copyFile(join(basePath, file), `./.${file}`);
  }

  await promisify(execFile)('npm', [
    'install',
    '-D',
    'eslint',
    'eslint-config-google',
    'eslint-config-prettier',
    'eslint-plugin-import',
    'eslint-plugin-jsdoc',
    'eslint-plugin-prettier',
    'prettier-eslint',
    'prettier-eslint-cli',
    '@typescript-eslint/eslint-plugin',
    '@typescript-eslint/parser',
  ]);

  for (const file of [
    'jsconfig.json',
    'verify.jsconfig.json',
    'declaration.jsconfig.json',
    'package.json',
  ]) {
    await promisify(execFile)('npx', ['prettier-eslint-cli', '--write', file]);
  }
}

main().catch((err) => {
  /* eslint-disable-next-line no-console */
  console.log(err);
  process.exit(1);
});
